package com.example.fluentd.controller;

import org.fluentd.logger.FluentLogger;
import org.fluentd.logger.FluentLoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.slf4j.event.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * FluentdController
 *
 * @author ChrisV_G
 * @since 02/11/2018
 */
@RestController
@RequestMapping("/fluentd-logs")
public class FluentdController {
    private static final Logger LOG = LoggerFactory.getLogger(FluentdController.class);

    @Autowired
    private FluentLogger logger ;
    private final String LABEL="fluent";
    @Autowired
    RestTemplate restTemplate;

    private static final org.slf4j.Logger Logger =
            org.slf4j.LoggerFactory.getLogger(FluentdController.class);
    /**
     * Metodo de prueba para realizar almacenado de logs en elasticsearch usando fluentD
     *
     * @return getRandomMessage
     */
    @RequestMapping(value = "getRandomMessage", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public ResponseEntity<?> getRandomMessage() {
        try {
            ResponseEntity response=restTemplate.exchange("http://api.icndb.com/jokes/random",HttpMethod.GET, null,Map.class);
            logger.log(LABEL,"FluentdController.getRandomMessage() ->  ",response.getBody().toString());
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(LABEL,"FluentdController.getRandomMessage() ->  ",e.getLocalizedMessage());
            return ResponseEntity.badRequest().build();
        }
    }


}
